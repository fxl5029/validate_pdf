package org.apache.nifi.processors.first;


import org.apache.nifi.expression.ExpressionLanguageScope;

import org.apache.nifi.logging.ComponentLog;
import org.apache.nifi.processor.*;
import org.apache.nifi.processor.io.InputStreamCallback;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

import org.apache.nifi.components.PropertyDescriptor;
import org.apache.nifi.flowfile.FlowFile;
import org.apache.nifi.annotation.behavior.ReadsAttribute;
import org.apache.nifi.annotation.behavior.ReadsAttributes;
import org.apache.nifi.annotation.behavior.WritesAttribute;
import org.apache.nifi.annotation.behavior.WritesAttributes;
import org.apache.nifi.annotation.lifecycle.OnScheduled;
import org.apache.nifi.annotation.documentation.CapabilityDescription;
import org.apache.nifi.annotation.documentation.SeeAlso;
import org.apache.nifi.annotation.documentation.Tags;
import org.apache.nifi.processor.exception.ProcessException;
import org.apache.nifi.processor.AbstractProcessor;
import org.apache.nifi.processor.ProcessContext;
import org.apache.nifi.processor.ProcessSession;
import org.apache.nifi.processor.ProcessorInitializationContext;
import org.apache.nifi.processor.Relationship;
import org.apache.nifi.processor.util.StandardValidators;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.apache.pdfbox.preflight.PreflightDocument;
import org.apache.pdfbox.preflight.ValidationResult;
import org.apache.pdfbox.preflight.exception.SyntaxValidationException;
import org.apache.pdfbox.preflight.parser.PreflightParser;

// import org.apache.pdfbox.pdfparser.NonSequentialPDFParser;
import org.apache.pdfbox.preflight.utils.ByteArrayDataSource;
import javax.activation.DataSource;

@Tags({"PDF", "validation"})
@CapabilityDescription("Validates PDF for incoming flow files")
@WritesAttributes({
        @WritesAttribute(attribute = "validatepdf.invalid.error", description = "If the flow file is routed to the invalid relationship "
                + "the attribute will contain the error message resulting from the validation failure.")
})
public class ValidatePDF extends AbstractProcessor {

    public static final PropertyDescriptor PDF_FILE = new PropertyDescriptor.Builder()
            .name("PDF File")
            .description("The path to the PDF file that is to be used for validation")
            .required(true)
            .expressionLanguageSupported(ExpressionLanguageScope.VARIABLE_REGISTRY)
            .addValidator(StandardValidators.FILE_EXISTS_VALIDATOR)
            .build();

    public static final Relationship VALID = new Relationship.Builder()
            .name("valid")
            .description("FlowFiles that are successfully validated against the PDF are routed to this relationship")
            .build();
    public static final Relationship INVALID = new Relationship.Builder()
            .name("invalid")
            .description("FlowFiles that are not valid according to the specified PDF are routed to this relationship")
            .build();


    private List<PropertyDescriptor> descriptors;

    private Set<Relationship> relationships;

    @Override
    protected void init(final ProcessorInitializationContext context) {
        final List<PropertyDescriptor> descriptors = new ArrayList<PropertyDescriptor>();
        descriptors.add(PDF_FILE);
        this.descriptors = Collections.unmodifiableList(descriptors);

        final Set<Relationship> relationships = new HashSet<Relationship>();
        relationships.add(VALID);
        relationships.add(INVALID);
        this.relationships = Collections.unmodifiableSet(relationships);
    }

    @Override
    public Set<Relationship> getRelationships() {
        return this.relationships;
    }

    @Override
    public final List<PropertyDescriptor> getSupportedPropertyDescriptors() {
        return descriptors;
    }


    @OnScheduled
    public void parseSchema(final ProcessContext context) {

    }

    @Override
    public void onTrigger(final ProcessContext context, final ProcessSession session) throws ProcessException {
        FlowFile flowFile = session.get();
        if ( flowFile == null ) {
            return;
        }

        final ValidationResult[] result = {null};
        final AtomicReference<Exception> exception = new AtomicReference<Exception>(null);


        session.read(flowFile, new InputStreamCallback() {

            @Override
            public void process(InputStream in) throws IOException {

                try {
                    final PreflightParser parser = new PreflightParser((DataSource) new ByteArrayDataSource(in));
                    parser.parse();
                    PreflightDocument document = parser.getPreflightDocument();
                    document.validate();
                    result[0] = document.getResult();

                } catch (SyntaxValidationException e)
                {
                    result[0] = e.getResult();
                }

            }
        });

        if (result[0].isValid()) {

            session.transfer(flowFile, VALID);
        } else {
            // flowFile = session.putAttribute(flowFile, "validatepdf.invalid.error", exception.get().getLocalizedMessage());
            session.transfer(flowFile, INVALID);
        }


    }
}
